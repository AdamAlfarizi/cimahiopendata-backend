<?php

namespace App\Http\Controllers;

use App\Permohonan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PermohonanController extends Controller
{

    public function index(){
        $permohonan = Permohonan::select('id_permohonan_data', 'deskripsi', 'status', 'dibalas', 'pembalas', 'balasan', 'masyarakat_id', 'kode_unor', 'created_at', 'updated_at')->get();
        return response()->json($permohonan);
    }

    public function store(Request $request){
        // $permohonan = new Permohonan();
        // $permohonan->deskripsi = $request->deskripsi;
        // $permohonan->masyarakat_id = $request->masyarakat_id;
        if($request->isMethod('post')){

            $this->validate($request, [
                'masyarakat_id' => 'required',
                'deskripsi' => 'required',
            ]);
            
            $data = $request->all();
            $insert = Permohonan::create($data);

            if($insert){
                $status = [
                    "pesan" => "Berhasil Mengajukan Permohonan",
                    "results" => $data,
                    "code" => 200
                ];
            }else{
                $status = [
                    "pesan" => "Gagal Mengajukan Permohonan",
                    "results" => $data,
                    "code" => 200
                ];
            }
            return response()->json($status, $status['code']);
        }

        $input = $request->all();
        return response()->json($input, 200);


    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
}
