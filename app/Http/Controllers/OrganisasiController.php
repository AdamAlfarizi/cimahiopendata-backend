<?php

namespace App\Http\Controllers;

use App\Organisasi;
use App\DataSetDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class OrganisasiController extends Controller
{

    public function index(){
        $organisasi = Organisasi::select('id_organisasi', 'nama_organisasi', 'gambar_organisasi','deskripsi_organisasi')->take(8)->get();
        return response()->json($organisasi);
    }

    public function allOrganisasi(){
        $organisasi = Organisasi::select('id_organisasi', 'nama_organisasi', 'gambar_organisasi','deskripsi_organisasi')->get();
        return response()->json($organisasi);
    }

    public function searchByOrganisasi($b){
        $datasetdetail = DataSetDetail::select('id_dataset_detail', 'nama_dataset_detail', 'file', 'ektensi', 'created_at', 'updated_at', 'dataset_id')->where('sumber', $id)->get();
        if(!empty($datasetdetail)){
            return response()->json($datasetdetail);
        }else{
            $datasetdetail = array("Data not Found!");
            return response()->json($datasetdetail);
        }
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
}
