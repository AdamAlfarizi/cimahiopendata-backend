<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/version', function () use ($router) {
    return $router->app->version();
});
$router->get('/', 'ExampleController@index');
$router->get('/topik', 'TopikController@index');
$router->get('/topik/all', 'TopikController@allTopik');

$router->get('/organisasi', 'OrganisasiController@index');
$router->get('/organisasi/all', 'OrganisasiController@allOrganisasi');

$router->get('/dataset', 'DataSetController@index');
$router->get('/dataset/all', 'DataSetController@allDataset');
$router->get('/dataset/detail/{id}', 'DataSetController@getDataset');
$router->get('/dataset/search/tags', 'DataSetController@searchDataset');
$router->get('/dataset/search/id', 'DataSetController@searchDataset');

$router->get('/dataset/search/b', 'DataSetController@searchDataset');
$router->get('/data/org/{s}', 'DataSetController@orgData');

$router->get('/permohonan', 'PermohonanController@index');
$router->post('/permohonan/store', 'PermohonanController@store');

$router->post('/masyarakat/login', 'MasyarakatController@login');

