<!DOCTYPE html>
<html lang="en">
<head>
    <title>CIMAHI OPEN DATA API</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1 class="display-3">OPEN DATA CIMAHI API</h1>
            </div>
        </div>

        <div class="row justify-content-center mt-4">
        <a class="text-decoration-none" href="/dataset/all"> <Button class="btn btn-primary">Dataset All</Button>
        </a>
        </div>
        <div class="row justify-content-center mt-4">
        <a class="text-decoration-none" href="/dataset"> <Button class="btn btn-primary">Dataset Return 6 data</Button>
        </a>
        </div>
        <div class="row justify-content-center mt-4">
        <a class="text-decoration-none" href="/topik/all"> <Button class="btn btn-primary">Topik All</Button>
        </a>
        </div>
        <div class="row justify-content-center mt-4">
        <a class="text-decoration-none" href="/topik"> <Button class="btn btn-primary">Topik Return 2 data</Button>
        </a>
        </div>
        <div class="row justify-content-center mt-4">
        <p>Using Search : </p>
        </div>
        <div class="row justify-content-center mt-4">
        <a class="text-decoration-none" href="/dataset/search/?search=pendidikan"> <Button class="btn btn-primary">Example ?search=pendidikan</Button>
        </a>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>