<?php

namespace App\Http\Controllers;

use App\DataSet;
use App\DataSetDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Response;

class DataSetController extends Controller
{

    public function index(){
        $dataset = DataSet::select('id_dataset', 'nama_dataset', 'deskripsi_dataset', 'tahun', 'tags_dataset')->whereYear('created_at', '>', '2018')->take(6)->get();
        return response()->json($dataset);
    }

    public function allDataset(){
        $dataset = DataSet::select('id_dataset', 'nama_dataset', 'deskripsi_dataset', 'tahun', 'tags_dataset')->orderBy('created_at', 'ASC')->get();
        return response()->json($dataset);
    }

    public function getDataset($id){
        $datasetdetail = DataSetDetail::select('id_dataset_detail', 'nama_dataset_detail', 'file', 'ektensi', 'created_at', 'updated_at', 'dataset_id')->where('dataset_id', $id)->get();
        if(!empty($datasetdetail)){
            return response()->json($datasetdetail);
        }else{
            $datasetdetail = array("");
            return response()->json($datasetdetail);
        }
    }

    public function orgData($b){
        $datasetdetail = DataSetDetail::select('id_dataset_detail', 'nama_dataset_detail', 'file', 'ektensi', 'created_at', 'updated_at', 'dataset_id')->where('dataset_id', $b)->get();
        if(!empty($datasetdetail)){
            return response()->json($datasetdetail);
        }else{
            $datasetdetail = array("");
            return response()->json($datasetdetail);
        }
    }


    public function searchDataset(Request $request){
        $s = $request->tags;
        $id = $request->id;
        $b = $request->b;
        if (!empty($request)) {
            if(!empty($s)){
                $dataset = DataSet::select('id_dataset', 'nama_dataset', 'deskripsi_dataset', 'tahun', 'tags_dataset')->where('tags_dataset', 'like', "%".$s."%")->get();
                return response()->json($dataset);
            }elseif(!empty($id)){
                $dataset = DataSet::select('id_dataset', 'nama_dataset', 'deskripsi_dataset', 'tahun', 'topik_id')->where('topik_id', $id)->get();
                return response()->json($dataset);
            }elseif(!empty($b)){
                $dataset = DataSet::select('id_dataset', 'nama_dataset', 'deskripsi_dataset', 'tahun', 'sumber')->where('sumber', $b)->get();
                return response()->json($dataset);
            }
            else{
                $dataset = array();
            return response()->json($dataset);
            }
        }else{
            $dataset = array();
            return response()->json($dataset);
        }
        }








    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
}
