<?php

namespace App\Http\Controllers;

use App\Masyarakat;
use App\Permohonan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class MasyarakatController extends Controller
{

    public function index(){
        $permohonan = Permohonan::select('id_permohonan_data', 'deskripsi', 'status', 'dibalas', 'pembalas', 'balasan', 'masyarakat_id', 'kode_unor', 'created_at', 'updated_at')->get();
        return response()->json($permohonan);
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = Masyarakat::where("email", $email)->first();
        
        $masyarakat = Masyarakat::select('id','nama', 'email')->where('email', $email)->get();

        if (!$user) {
            $status =[
                "pesan" => "Email/Password salah !",
                "code" => 401,
            ];

            return response($status, $status['code']);
        } else {
           if(Hash::check($password, $user->password)){
            $status =[
                "pesan" => "Berhasil Login",
                "code" => 200,
                "masyarakat" => $masyarakat
            ];

            return response($status, $status['code']);
           }else{
            $status =[
                "pesan" => "Password Salah",
                "code" => 401,
            ];

            return response($status, $status['code']);
           }
        }
        
        

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
}
