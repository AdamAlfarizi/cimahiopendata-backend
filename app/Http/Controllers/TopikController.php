<?php

namespace App\Http\Controllers;

use App\Topik;
use App\DataSetDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TopikController extends Controller
{

    public function index(){
        $topik = Topik::select('id_topik', 'nama_topik', 'gambar_topik')->take(9)->get();
        return response()->json($topik);
    }

    public function allTopik(){
        $topik = Topik::select('id_topik', 'nama_topik', 'gambar_topik')->get();
        return response()->json($topik);
    }

    public function searchByTopik($id){
        $datasetdetail = DataSetDetail::select('id_dataset_detail', 'nama_dataset_detail', 'file', 'ektensi', 'created_at', 'updated_at', 'dataset_id')->where('topik_id', $id)->get();
        if(!empty($datasetdetail)){
            return response()->json($datasetdetail);
        }else{
            $datasetdetail = array("Data not Found!");
            return response()->json($datasetdetail);
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
}
